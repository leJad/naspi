**Authenticate Yourself**
----
  To use spotify API, you first need to authenticate yourself. So do it.

* **URL**

  /spotify/

* **Method:**

  `GET`
  
*  **URL Params**

  None.

* **Redirect Response:**

  * **Code:** 302 <br />

* **Notes:**

  But first of all, please change your clientId and clientSecret in your .env file <br />
  If you don't have Id or Secret, Create an application [here](https://developer.spotify.com/my-applications) to get them.


**Change your status to "Listening now"**
----
  Client will change your whatsapp status to whatever song you are listening to on spotify. 

* **URL**

  /spotify/listen

* **Method:**

  `GET`
  
*  **URL Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `We will update your status as long as you stay here`

