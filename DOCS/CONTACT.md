**Get Profile Picture**
----
  Get profile picture of someone.

* **URL**

  /contact/pfp/:number

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `number=[string]`

* **Data Params**

  None

* **Redirect Response:**

  * **Code:** 302
 

**Is registered**
----
  Check if the number is registered to whatsapp or not

* **URL**

  /contact/isregistered/:number

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `number=[string]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `yes.`

  OR

  * **Code:** 200 <br />
    **Content:** `no.`


**New Status**
----
  Change your status to new one

* **URL**

  /contact/status

* **Method:**

  `POST`

* **Data Params**

  * **Content-Type**: `application/json` <br />
    **Data**: `{"status": "I hate everything"}`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ Success: "New status: < status > " }`
 

* **Sample Call:**

  ```curl
  curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"status": "I hate everything"}' \
  http://localhost:8000/contact/status
  ```

**Get Contacts**
----
  Render your contacts in a html table

* **URL**

  /contact/

* **Method:**

  `GET`
  
*  **URL Params**

  None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `Rendered ejs file`
