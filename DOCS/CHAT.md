**Set Automated Message**
----
  Set an automated message that client can reply with whenever we recieve a message ( not group message )

* **URL**

  /chat/auto

* **Method:**

  `POST`

* **Data Params**
  
  * **Content-Type**: `application/json` <br />
    **Data**: `{"message": "This is an automated message!"}`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{Succes: I will respond with < automatedMessage > }`

* **Sample Call:**

  ```curl
  curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"test":"test"}' \
  http://localhost:8000/chat/auto
  ```



**Send Message**
----
  Send message to someone

* **URL**

  /chat/sendmsg

* **Method:**

  `POST`

* **Data Params**
  
  * **Content-Type**: `application/json` <br />
    **Data**: `{"number": "Example number", "message": "Hi mom!"}`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{Success: `Message sent to < number >}`

* **Error Response:**

  * **Content:** `{Error: "This number doesn't use whatsapp."}`

* **Sample Call:**

  ```curl
  curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"number":"Example number", "message": "Hi mom!"}' \
  http://localhost:8000/chat/sendmsg
  ```