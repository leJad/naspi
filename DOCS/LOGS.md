**Add log to DB**
----
  Adds new log to the database (logs.json)

* **URL**

  /logs/add

* **Method:**

  `POST`
  
* **Data Params**

  * **Content-Type**: `application/json` <br />
    **Data**: `{"deletedMessage": "ur gay lmao"}`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ "Logs": "ur gay lmao" }`

* **Sample Call:**

  ```curl
  curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"deletedMessage": "ur gay lmao"}' \
  http://localhost:8000/logs/add
  ```

* **Notes:**

  I wouldn't recommend you to add new logs manually because 
  
  * The fucking API already does it itself, don't fuck with my code.
  * This is just stupid. Don't do it.


**Get Logs**
----
  Render everything (I mean deleted messages) from logs.json

* **URL**

  /logs/

* **Method:**

  `GET`
  
*  **URL Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `Deleted Messages`