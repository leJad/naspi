**Get QR**
----
  Renders a QR Code so you can scan it with your phone and authenticate the client.

* **URL**

  /auth/

* **Method:**

  `GET`

*  **URL Params**
  
  None

* **Data Params**

  None

* **Notes:**

  There's nothing too crazy here, just go to localhost/auth/ and scan the qr code