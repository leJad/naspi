**Add message and numbers to database**
----
  Think of this as a control panel or something for bulk route.
  You insert numbers and messages then write them to database.

* **URL**

  /bulk/

* **Method:**

  `GET`
  
*  **URL Params**

  None.

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `2 Forms to make post requests. One for numbers, other for messages`


**Add new number**
----
  Add new number to database

* **URL**

  /bulk/number

* **Method:**

  `POST`

* **Data Params**

  * **Content-Type**: `application/json` <br />
    **Data**: `{"number": "Example number"}`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ Success : "Number added to database!" }`
 
* **Error Response:**

  *  **Content:** `{ Error: "This number doesn't use whatsapp." }`


* **Sample Call:**

  ```curl
  curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"number": "Example number"}' \
  http://localhost:8000/bulk/message
  ```



**Add new message**
----
  Add the message which we will use to send to every number you registered to database.

* **URL**

  /bulk/message

* **Method:**

  `POST`
  
* **Data Params**

  * **Content-Type**: `application/json` <br />
    **Data**: `{"message": "Finna commit war crimes!"}`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ Success : "All cool." }`

* **Sample Call:**

  ```curl
  curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"message": "Finna commit war crimes!"}' \
  http://localhost:8000/bulk/message
  ```



**Delete number**
----
  Have you inserted a wrong number and are too fucking lazy to delete it manually?
  Well then worry not my friend. Because we got you covered!

* **URL**

  /bulk/delete/:number

* **Method:**

  `DELETE`
  
*  **URL Params**

   **Required:**
 
   `number=[string]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ Success: "Yass! That motherfucker is gone now!" }`
 
* **Sample Call:**

  ```curl
  curl -X DELETE http://localhost:8000/bulk/delete/ExampleNumber
  ```


**Send message after you are done**
----
  Send message to numbers which you added to database earlier

* **URL**

  /bulk/send

* **Method:**

  `GET`
  
*  **URL Params**

  None

* **Success Response:**
  
  <_What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!_>

  * **Code:** 200 <br />
    **Content:** `{ Success: "Its all done now." }`
 