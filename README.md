[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]



<!-- PROJECT LOGO -->
<br />
<p align="center">

  <h3 align="center">naspi</h3>

  <p align="center">
    Even I don't know what this project is about.
    <br />
    <a href="https://github.com/leJad/naspi"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://github.com/leJad/naspi/issues">Report Bug</a>
    ·
    <a href="https://github.com/leJad/naspi/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project


About a week ago, while I was doing the usual things, something occurred to me. <br />
Why am I not messing with the WhatsApp API? <br /> 
The answer is obvious, because such a thing is not available to the public. So I decided to make my own.


Here's why:
* I was bored.
* I wanted to let everyone know that I'm leaving WhatsApp soon. However, I did not want to text everyone the same message over and over again MANUALLY.

### Built With

* [NodeJS](https://nodejs.org/en/)
* [Express](https://www.npmjs.com/package/express)
* [Spotify API](https://www.npmjs.com/package/spotify-web-api-node)
* [StormDB](https://www.npmjs.com/package/stormdb)
* [Whatsapp Web API](https://github.com/pedroslopez/whatsapp-web.js/)


<!-- GETTING STARTED -->

### Prerequisites

Before downloading this project with git, you should install NodeJS and Git
* Install Node from [here](https://nodejs.org/en/)
* Install Git from [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

### Installation

1. Clone the repo
   ```sh
   git clone https://github.com/leJad/naspi.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Enter your Spotify App clientId and clientSecret in `.env` (skip this if you don't want to use spotify route)
   ```s
   clientId = Your client id
   clientSecret = Your client secret

   # If you don't have Id or Secret, Create an application here: https://developer.spotify.com/my-applications to get them.

   # Also change the name of this file from .env.example to .env
   ```

4. CD into src directory
  ```sh
  cd src/
  ```

5. Run server
  ```sh
  node main
  ```

6. Finally, authenticate client on /auth/

<!-- USAGE EXAMPLES -->
## Docs

For more examples, please refer to the [Documentation](https://github.com/leJad/naspi/tree/main/DOCS)



<!-- ROADMAP -->
## Roadmap

See the [open issues](https://github.com/leJad/naspi/issues) for a list of proposed features (and known issues).



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.


<!-- CONTACT -->
## Contact

My blog - [Let's Talk!](https://ecma.netlify.app/)


<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [Best README Template](https://github.com/othneildrew/Best-README-Template)
* [Whatsapp Web API](https://github.com/pedroslopez/whatsapp-web.js/)
* [Spotify Web API](https://www.npmjs.com/package/spotify-web-api-node)



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/leJad/naspi.svg?style=for-the-badge
[contributors-url]: https://github.com/leJad/naspi/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/leJad/naspi.svg?style=for-the-badge
[forks-url]: https://github.com/leJad/naspi/network/members
[stars-shield]: https://img.shields.io/github/stars/leJad/naspi.svg?style=for-the-badge
[stars-url]: https://github.com/leJad/naspi/stargazers
[issues-shield]: https://img.shields.io/github/issues/leJad/naspi.svg?style=for-the-badge
[issues-url]: https://github.com/leJad/naspi/issues
[license-shield]: https://img.shields.io/github/license/leJad/naspi.svg?style=for-the-badge
[license-url]: https://github.com/leJad/naspi/blob/master/LICENSE
