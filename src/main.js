"use strict";

const Express = require("express");
const app = Express();
const compression = require("compression");
const fs = require("fs");
const { Client } = require("whatsapp-web.js");
const axios = require("axios");

//Import routes
const contact = require("./routes/contact.js");
const auth = require("./routes/auth.js");
const chat = require("./routes/chat.js");
const bulk = require("./routes/bulk.js");
const logs = require("./routes/logs.js");
const spotify = require("./routes/spotify.js");

/**
 * Storing sessions in a json file because we don't want the user to scan a qr code everytime.
 */
const sessionPath = "./database/sessions.json";

/**
 * Middlewares
 */
app.use(compression());
app.use(isReady);

/**
 * Set view engine as EJS so we can render them. See /views
 */
app.set("view engine", "ejs");
app.use(Express.static(__dirname + "/styles")); /* To use css */

let sessionData;
let ready;

/**
 * Set automatedMessage as a global variable. If you don't understand why I need this, check routes/message.js.
 * Basically when a user makes GET request to chat/auto/<message>, We assign <message> to automatedMessage.
 * And whenever someone sends us a message we reply to them with the automatedMessage.
 */
global.automatedMessage;

/**
 * Set authenticated as a global variable 
 * so we can let the auth route know if we are already authenticated or not 
 */
global.authenticated = false;

if (fs.existsSync(sessionPath)) {
  sessionData = require(sessionPath);
}

/**
 * Set client as a global variable because we want to use it in other files as well.
 */
global.client = new Client({
  session: sessionData,
});

client.on("qr", (qr) => {
  /**
   * Fired when the qr code is created. 
   * We will wait for the user to scan the qr code and then mark user as authenticated.
   * See: ./routes/auth.js
   */

  global.qr = qr;
  ready = true
  console.log("Please scan the QR code");
});

client.on("authenticated", async (session) => {
  /**
   * If user is authenticated write session to ./database/sessions.json
   */
  sessionData = session;
  authenticated = true;

  fs.writeFile(sessionPath, JSON.stringify(session), (err) => {
    if (err) throw err;
  });
});

client.on("ready", () => {
  /**
   * Fired when the client is ready.
   */

  /** 
   * If we don't set this to true, user will not be able to do shit because of the isReady middleware
   * Now you may think "Fuck this, I don't want this variable and that route to be in my code. I'm deleting them!"
   * If you delete them an error will occur. And its because if the client is not ready and the user wants to send a message to an old friend (fucking impatient)
   * Instead of sending a message to its good ol' friend, they will encounter a small error 
   */
  ready = true; 
  console.log("Client is ready!");
});

client.on("message", async (msg) => {
  let chat = await msg.getChat();
  /**
   * Whenever someone sends client a message client responds with an automated message if the user has set it up
   */
   if (chat.isGroup){
     return; 
     /** 
      * We don't want our client to reply to group chats. 
      * Imagine a group chat. People are chatting, texting etc. and then there's you replying to their every message with "Oh so sorry but I am busy as fuck right now, so maybe talk later?"
      * So yeah. We don't want this to happen. 
      */
   }

  typeof automatedMessage != "undefined"
    ? msg.reply(automatedMessage)
    : console.log(
        "You recieved a message, but didn't create an automated ones"
      );
});

/**
 * If someone deletes a message
 * Log who deleted the message and the deleted message itself in a .json file (/database/logs.json)
 */
client.on("message_revoke_everyone", async (after, before) => {
  // Fired whenever a message is deleted by anyone (including you)
  if (before) {
    postRequest(`${before.from.replace("@c.us", "")} Deleted: ${before.body}`); // message before it was deleted
  }
});

client.initialize();

/**
 * Use routes
 */
app.use("/contact", contact);
app.use("/auth", auth);
app.use("/chat", chat);
app.use("/bulk", bulk);
app.use("/logs", logs);
app.use("/spotify", spotify)

/**
 * We will use this function to
 * Post the deleted message to .json file (/database/logs.json)
 */
function postRequest(deletedMessage) {
  axios.request({
    method: "POST",
    url: "http://localhost:8000/logs/add",
    headers: { "Content-Type": "application/json" },
    data: {
      message: deletedMessage,
    },
  });
}

/**
 * Tell the user that client is not ready yet. Jeez, have some fucking patience.
 * This is uber super duper important don't delete!
 */
function isReady(req, res, next) {
  if (typeof ready == "undefined") {
    res.send("Client is not ready yet. Please be patient.");
  }
  next();
}

app.listen(8000, () => console.log("HTTP Server up on port 8000"));

/*
   _____      ____
  /\/\/\/\   | "o \ 
<|\/\/\/\/|_/ /___/
 |___________/     
 |_|_|  /_/_/

 */