const Express = require("express");
const StormDB = require("stormdb");

const engine = new StormDB.localFileEngine("./database/logs.json");
const db = new StormDB(engine);

const router = Express.Router();

router.use(Express.json());

/**
 * Set default value for our database
 */

db.default({
  Deleted: [] /* Yes I keep it as an array. Yes in an object. */,
});

router.post("/add", (req, res) => {
  /**
   * When someone deletes a message (See main.js -> Code line 126)
   * Add that message to logs.json
   */

  let bodyMessage = Object.values(req.body)[0];

  db.get("Deleted").push(bodyMessage).save();

  res.json({ Logs: db.get("Deleted").value });
});

router.get("/", (req, res) => {
  /**
   * Render contents of logs.json (Which are deleted messages, obviously.)
   */

  let log = db.get("Deleted").value();

  res.render("logs", { log });
});

module.exports = router;
