const Express = require("express");
const qrcode = require("qrcode");
const router = Express.Router();

router.get("/", (req, res) => {
  if (authenticated) {
    /**
     * Tell the user that they are authenticated (We don't want them to scan qr code if they are already authenticated).
     */
    res.send(`<script>alert("You are already authenticated!")</script>`);
  } else if (qr == undefined) {
    /**
     * Hang user for 3 seconds if we still don't have any qr code to show
     */
    res.send(waitPage);
  } else {
    /**
     * If user is not authenticated && we have the qr code, render qr code as png and wait for the user to scan it. See views/qrcode.ejs
     */
    qrcode.toDataURL(qr, (err, url) => {
      res.render("qrcode", { url });
      if (err) throw err;
    });
  }
});

const waitPage = `
      <h1>Please wait...</h1>
      <script>
        setTimeout(() => location.reload(), 3000)
      </script>
    `;

module.exports = router;

/**
 * Note
 * Code in this file is so fucking ugly and I couldn't make it look better (I can't even look at it, I created a fucking monster). So if you know how to fix my mess please, do it.
 * total_hours_wasted_here = 42
 */
