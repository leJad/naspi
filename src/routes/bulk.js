const Express = require("express");
const StormDB = require("stormdb");

const engine = new StormDB.localFileEngine("./database/database.json");
const db = new StormDB(engine);

const router = Express.Router();
router.use(Express.urlencoded({ extended: "true" }));

/**
 * Set default values for database
 */

db.default({
  Numbers:
    [] /* Again, yes we will keep it in an array, inside of an object. What did you except? */,
  Message:
    "Default Message" /* We will send only ONE message to each number, so we keep it as string */,
});

router.get("/", (req, res) => {
  /**
   * Think of this as a control panel or something for bulk route.
   * You insert numbers and messages then write them to database.
   */

  res.render("bulk");
});

router.post("/number", (req, res) => {
  let number = Object.values(req.body)[0];

  /**
   * Check if the user is registered to whatsapp
   */

  client.isRegisteredUser(`${number}@c.us`).then((is) => {
    if (is) {
      /* Save number to database */

      db.get("Numbers").push(number).save();

      res.json({ Success: "Number added to database!" });
    } else {
      res.json({
        Error: `This number is not registered to user. The number you inserted in: ${number}`,
      });
    }
  });
});

router.post("/message", (req, res) => {
  let message = Object.values(req.body)[0];

  db.get("Message").set(message).save();

  res.json({ Success: "All cool." });
});

router.delete("/delete/:id", (req, res) => {
  /**
   * Have you inserted a wrong number and are too fucking lazy to delete it manually?
   * Well then worry not my friend. Because we got you covered!
   */

  let number = db.get("Numbers");
  let id = req.params.id; /* Don't let the name fool you. You will insert the number */

  /* Find the ID of number */
  let indexOfNumber = number.value().indexOf(id).toString();
  
  /* Then use that ID to find an delete that little fucker. */
  number.get(indexOfNumber).delete(true);

  db.save(); /* We can save our db after we are done, Yay! */

  res.json({ Success: "Yass! That motherfucker is gone now!" });

});

router.get("/send", async (req, res) => {
  /**
   * Final part
   * Send the message to the users which are saved in logs.json
   */

  let numbers = db.get("Numbers");
  let message = db.get("Message").value();

  numbers.value().forEach((nums) => {
    client.sendMessage(`${nums}@c.us`, message);
  });

  res.json({ Success: "Its all done now." });
});

module.exports = router;
