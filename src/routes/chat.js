const Express = require("express");

const router = Express.Router();
router.use(Express.json());

router.post("/auto/", async (req, res) => {
  /**
   * Set an automated message that client can reply with when someone sends a message
   */

  global.automatedMessage = Object.values(req.body)[0];
  res.json({Succes: `I will respond with ${automatedMessage}`})
});

router.post("/sendmsg", async (req, res) => {
  /**
   * First, check if the number is registered to whatsapp. If so, send message to that number
   */
  
  let number = Object.values(req.body)[0]
  let message = Object.values(req.body)[1]

  client.isRegisteredUser(`${number}@c.us`).then((is) => {
    if (is) {
      client.sendMessage(`${number}@c.us`, message);
      res.json({Success: `Message sent to ${number}`})
    } 
    
    else {
      res.json({Error: "This number doesn't use whatsapp."});
    }
  });
});

module.exports = router;
