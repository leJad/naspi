const Express = require("express");

const router = Express.Router();

router.get("/", async (req, res) => {
  /**
   * Shows cotacts. Their name and number (Will not show number if its a group, obviously).
   */

  client.getContacts().then((contacts) => {
    res.render("contacts", { contacts });
  });
});

router.get("/pfp/:phone", async (req, res) => {
  /**
   * Get the profile picture of given phone and redirect user to the pfp url
   */

  client.getProfilePicUrl(`${req.params.phone}@c.us`).then((url) => {
    url
      ? res.redirect(url)
      : res.send("Oopsie Daisy. Something fucked up our API");
  });
});

router.post("/status/", async (req, res) => {
  /**
   * Set new status
   */

  let newStatus = Object.values(req.body)[0];

  await client.setStatus(newStatus);

  res.json({ Success: `New status: ${newStatus}` });
});

router.get("/isregistered/:number", (req, res) => {
  /**
   * Just checking if the number is registered to whatsapp or not.
   */

  let number = req.params.number;

  client.isRegisteredUser(`${number}@c.us`).then((is) => {
    is ? res.send("yes.") : res.send("no.");
  });
});

module.exports = router;
