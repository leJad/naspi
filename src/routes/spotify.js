const SpotifyWebApi = require("spotify-web-api-node");
const Express = require("express");
require('dotenv').config({ path: "../.env" })

const router = Express.Router();
let token;


/* Don't forget to edit your .env file! */
let clientId = process.env.clientId
let clientSecret = process.env.clientSecret

/**
 * If you don't have a client_id and client_secret yet,
 * Create an application here: https://developer.spotify.com/my-applications to get them.
 */

const spotifyApi = new SpotifyWebApi({
  clientId: clientId,
  clientSecret: clientSecret,
  redirectUri: "http://localhost:8000/spotify/callback",
});

const scopes = [
  "ugc-image-upload",
  "user-read-playback-state",
  "user-modify-playback-state",
  "user-read-currently-playing",
  "streaming",
  "app-remote-control",
  "user-library-modify",
  "user-library-read",
  "user-top-read",
  "user-read-playback-position",
  "user-read-recently-played",
];

router.get("/", (req, res) => {
  /**
   * Redirect to authorization page
   */
  res.redirect(spotifyApi.createAuthorizeURL(scopes));
});

router.get("/callback", (req, res) => {
  const code = req.query.code;

  spotifyApi.authorizationCodeGrant(code).then((response) => {
    /**
     * Set our access token to be able to interact with spotify api
     */

    token = response.body.access_token;
    spotifyApi.setAccessToken(token);

    res.send("Success! You can now close the window.");
  });
});

router.get("/listen", async (req, res) => {
  spotifyApi.getMyCurrentPlayingTrack().then(
    async (data) => {
      try {
        /**
         * Get current playing track and set it as user's whatsapp status.
         */
        let songName = data.body.item.name;
        await client.setStatus("Spotify ~ Now playing: " + songName);
        /**
         * Rendering spotify.ejs
         * So we can poll spotify api to check if the user is listening to another song (and then set it as new status lol)
         */
        res.render("spotify");
      } catch (err) {
        console.log(
          "An error has occured! \n Its probably because you are not listening to anything or you are not authenticated."
        );
        throw err;
      }
    },
    (err) => {
      console.log("Something went wrong!", err);
    }
  );
});

module.exports = router;
